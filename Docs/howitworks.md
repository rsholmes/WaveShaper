# How the VC 4046 wave shaper works

See Barton's [documentation](https://www.bartonmusicalcircuits.com/4046/documentation.pdf) for how the original design works.

In the original version, each of the 7 output signals from the CD4040 goes through a 100 nF capacitor, to remove DC offset, and an attenuator before connecting to a 100k input resistor to the final summing op amp stage. In this VC version the 7 CD4040 outputs are "stolen" from the main board and are sent to the VCA/pot/switch board via the ribbon cable, where they are used as logic control signals on CD4066 analog switches (U2, U3). The analog inputs to these switches are the 7 control voltages, which originate with either an external control voltage source or, if none is plugged in, a 10 V level from the LM4040 voltage reference (U5). There is an attenuator for each CV (RV1–6, RV8), and a diode (D1, D4–9) with current limiting resistor (R9–15) to block negative voltages from reaching the 4066 chips.

Each of the 4066 outputs then is equal to the corresponding control voltage when the logic signal is high, or is pulled to ground via a 100k pulldown resistor (R1–R7) when the logic signal is low. In other words, the ~12 V square waves from the 4017 are converted to square waves whose amplitudes are the control voltages; effectively the analog switches and pulldown resistors act as 7 VCAs for square waves. Each signal is then buffered using the TL074s (U1, U4) before being sent back to the main PCB, where they now go through those 7 capacitors (mounted on the potentiometer footprints, since the capacitor footprints were used to connect the ribbon cable, and the potentiometers are relocated to the VCA/pot/switch board) and on to the output mixing stage as before.

The feedback resistor on the output mixing stage is replaced by a potentiometer (RV7), for manual control of the gain.

The power header (J5) is located on the VCA/pot/switch board, with a pin socket/header pair (J11) carrying the power rails to the main board.
