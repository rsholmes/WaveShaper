# waveshaper.sch BOM

Mon 04 Oct 2021 12:10:51 AM EDT

Generated from schematic by Eeschema 5.1.10-88a1d61d58~90~ubuntu20.04.1

**Component Count:** 57

**For the main PCB use Barton's BOM with the following changes:**

* For 10 Ω resistors, substitute 0 Ω or jumpers.
* Omit 10 µF capacitors.
* Omit pots.
* For 0.1 µF capacitors obtain ones with long enough legs to allow bending them to 2.54 mm pitch.
* For power header use plain (unshrouded) 2x5 pin header.
* Omit jacks (they are included in the list below).
* Omit switch (it is included in the list below).
* Add 14 conductor ribbon cable with 14 pin IDC connector.

**The following are components for the auxiliary boards.**

| Refs | Qty | Component | Description | Vendor | SKU |
| ----- | --- | ---- | ----------- | ---- | ---- |
| C3, C4 | 2 | 10uF | Electrolytic capacitor | Tayda | A-4349 |
| C5, C6 | 2 | 100nF | Ceramic capacitor | Tayda | A-553 |
| D1, D2, D3, D4, D5, D6, D7, D8, D9 | 9 | 1N5817 | Schottky Barrier Rectifier Diode, DO-41 | Tayda | A-159 |
| J1 | 1 | 2x7 IDC header | Pin header 2.54 mm 2x7 | Tayda | A-3349 |
| J3, J4 | 2 | AudioJack2 | Audio Jack, 2 Poles (Mono / TS) | Tayda | A-1121 |
| J5, J11 | 2 | 2 pin Molex header | KK254 Molex header | Tayda | A-804 |
| J6 | 1 | EURO_PWR_2x5 | Pin header 2.54 mm 2x5 | Tayda | A-2939 |
| J7, J10, J12, J15, J16, J17, J18 | 7 | AudioJack2_SwitchT | Audio Jack, 2 Poles (Mono / TS), Switched T Pole (Normalling) | Tayda | A-1121 |
| J8 | 1 | 1x9 pin header | Pin header 2.54 mm 1x9 | Tayda | A-197 |
| J9 | 1 | 1x9 pin socket | Pin socket 2.54 mm 1x9 | Tayda | A-196 |
| J13 | 1 | 3 pin Molex header | KK254 Molex header | Tayda | A-805 |
| J14 | 1 | 2x5 pin socket | Pin socket 2.54 mm 2x5 | Tayda | A-1696 |
| R1, R2, R3, R4, R5, R6, R7 | 7 | 1M | Resistor | Tayda | A-2277 |
| R8, R9, R10, R11, R12, R13, R14, R15 | 8 | 1k | Resistor | Tayda | A-2200 |
| RV1, RV2, RV4, RV5, RV6, RV7, RV8 | 7 | B100k | 9 mm vertical board mount potentiometer | Tayda | A-1848 |
| RV3 | 1 | A100k | 9 mm vertical board mount potentiometer | Tayda | A-1855 |
| SW1 | 1 | SW_SPDT | Miniature toggle switch, single pole double throw, ON-OFF-ON | Tayda | A-3187 |
| U1, U4 | 2 | TL074 | Quad Low-Noise JFET-Input Operational Amplifiers, DIP-14 | Tayda | A-1138 |
| U2, U3 | 2 | CD4066 | Quad Analog Switches | Tayda | A-555 |
| U5 | 1 | LM4040DIZ-10 | 10.00V Precision Micropower Shunt Voltage Reference, TO-92 | DigiKey | LM4040DIZ-10.0/NOPB-ND |
| | 2 | 14 pin DIP socket || Tayda | A-003 |
| | 2 | 2 pin Molex connector | KK254 Molex connector | Tayda | A-826 |
| | 1 | 3 pin Molex connector | KK254 Molex connector | Tayda | A-827 |
| | 8 | Knob ||||
| | 1 | Ribbon cable | 14 conductor, ~10 cm | Tayda | A-4937 |
| | 1 | 2x7 IDC connector | Ribbon cable connector | Tayda | A-3349 |
