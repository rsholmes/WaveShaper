EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr USLetter 11000 8500
encoding utf-8
Sheet 1 2
Title "CV Wave Shaper "
Date "2020-07-07"
Rev ""
Comp "Analog Output / Rich Holmes "
Comment1 "Based on Barton 4046 wave shaper, with added CV "
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Sheet
S 8500 5850 1300 675 
U 5F1DEF77
F0 "Panel and power" 50
F1 "panelpower.sch" 50
$EndSheet
$Comp
L 4xxx:4066 U2
U 1 1 5F057496
P 4600 3850
F 0 "U2" V 4646 3722 50  0000 R CNN
F 1 "CD4066" V 4555 3722 50  0000 R CNN
F 2 "AO_tht:DIP-14_W7.62mm_Socket_LongPads" H 4600 3850 50  0001 C CNN
F 3 "http://www.ti.com/lit/ds/symlink/cd4066b.pdf" H 4600 3850 50  0001 C CNN
F 4 "Tayda" V 4600 3850 50  0001 C CNN "Vendor"
F 5 "A-555" V 4600 3850 50  0001 C CNN "SKU"
	1    4600 3850
	0    -1   -1   0   
$EndComp
$Comp
L 4xxx:4066 U3
U 2 1 5F05C1A3
P 4600 4650
F 0 "U3" V 4646 4522 50  0000 R CNN
F 1 "CD4066" V 4555 4522 50  0000 R CNN
F 2 "AO_tht:DIP-14_W7.62mm_Socket_LongPads" H 4600 4650 50  0001 C CNN
F 3 "http://www.ti.com/lit/ds/symlink/cd4066b.pdf" H 4600 4650 50  0001 C CNN
	2    4600 4650
	0    -1   -1   0   
$EndComp
Text GLabel 4600 5750 0    50   Input ~ 0
CV_-3
Text GLabel 4600 4950 0    50   Input ~ 0
CV_-1
Text GLabel 4600 4150 0    50   Input ~ 0
CV_+1
Text GLabel 4600 3350 0    50   Input ~ 0
CV_+3
$Comp
L 4xxx:4066 U3
U 1 1 5F064429
P 5700 4750
F 0 "U3" V 5746 4622 50  0000 R CNN
F 1 "CD4066" V 5655 4622 50  0000 R CNN
F 2 "AO_tht:DIP-14_W7.62mm_Socket_LongPads" H 5700 4750 50  0001 C CNN
F 3 "http://www.ti.com/lit/ds/symlink/cd4066b.pdf" H 5700 4750 50  0001 C CNN
F 4 "Tayda" V 5700 4750 50  0001 C CNN "Vendor"
F 5 "A-555" V 5700 4750 50  0001 C CNN "SKU"
	1    5700 4750
	0    -1   -1   0   
$EndComp
Wire Wire Line
	5150 3150 5400 3150
Wire Wire Line
	5150 3950 5400 3950
Wire Wire Line
	5150 4750 5400 4750
Text GLabel 5700 3450 0    50   Input ~ 0
CV_+2
Text GLabel 5700 4250 0    50   Input ~ 0
CV_0
Text GLabel 5700 5050 0    50   Input ~ 0
CV_-2
Wire Wire Line
	5150 5050 5150 4750
Wire Wire Line
	5150 4250 5150 3950
Wire Wire Line
	5150 3450 5150 3150
Wire Wire Line
	5700 2800 5700 2850
Wire Wire Line
	5700 3600 5700 3650
Wire Wire Line
	5700 4400 5700 4450
Wire Wire Line
	3900 3050 4300 3050
Wire Wire Line
	3850 5050 5150 5050
Wire Wire Line
	3900 4650 4300 4650
Wire Wire Line
	4000 3650 4000 4250
Wire Wire Line
	4000 4250 5150 4250
Wire Wire Line
	4050 3850 4300 3850
Connection ~ 6150 4400
Wire Wire Line
	5700 4400 6150 4400
$Comp
L power:GND #PWR014
U 1 1 5F07ECAB
P 6150 4700
F 0 "#PWR014" H 6150 4450 50  0001 C CNN
F 1 "GND" H 6155 4527 50  0000 C CNN
F 2 "" H 6150 4700 50  0001 C CNN
F 3 "" H 6150 4700 50  0001 C CNN
	1    6150 4700
	1    0    0    -1  
$EndComp
$Comp
L Device:R R7
U 1 1 5F04F04C
P 6150 4550
F 0 "R7" H 6220 4596 50  0000 L CNN
F 1 "100k" H 6220 4505 50  0000 L CNN
F 2 "AO_tht:R_Axial_DIN0207_L6.3mm_D2.5mm_P10.16mm_Horizontal" V 6080 4550 50  0001 C CNN
F 3 "~" H 6150 4550 50  0001 C CNN
	1    6150 4550
	1    0    0    -1  
$EndComp
Connection ~ 6150 3600
Wire Wire Line
	5700 3600 6150 3600
$Comp
L power:GND #PWR013
U 1 1 5F07F709
P 6150 3900
F 0 "#PWR013" H 6150 3650 50  0001 C CNN
F 1 "GND" H 6155 3727 50  0000 C CNN
F 2 "" H 6150 3900 50  0001 C CNN
F 3 "" H 6150 3900 50  0001 C CNN
	1    6150 3900
	1    0    0    -1  
$EndComp
$Comp
L Device:R R6
U 1 1 5F07C8DE
P 6150 3750
F 0 "R6" H 6220 3796 50  0000 L CNN
F 1 "100k" H 6220 3705 50  0000 L CNN
F 2 "AO_tht:R_Axial_DIN0207_L6.3mm_D2.5mm_P10.16mm_Horizontal" V 6080 3750 50  0001 C CNN
F 3 "~" H 6150 3750 50  0001 C CNN
	1    6150 3750
	1    0    0    -1  
$EndComp
Wire Wire Line
	5700 2800 6150 2800
$Comp
L power:GND #PWR012
U 1 1 5F07FC36
P 6150 3100
F 0 "#PWR012" H 6150 2850 50  0001 C CNN
F 1 "GND" H 6155 2927 50  0000 C CNN
F 2 "" H 6150 3100 50  0001 C CNN
F 3 "" H 6150 3100 50  0001 C CNN
	1    6150 3100
	1    0    0    -1  
$EndComp
$Comp
L Device:R R5
U 1 1 5F07C33D
P 6150 2950
F 0 "R5" H 6220 2996 50  0000 L CNN
F 1 "100k" H 6220 2905 50  0000 L CNN
F 2 "AO_tht:R_Axial_DIN0207_L6.3mm_D2.5mm_P10.16mm_Horizontal" V 6080 2950 50  0001 C CNN
F 3 "~" H 6150 2950 50  0001 C CNN
	1    6150 2950
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR09
U 1 1 5F07E897
P 5100 5450
F 0 "#PWR09" H 5100 5200 50  0001 C CNN
F 1 "GND" H 5105 5277 50  0000 C CNN
F 2 "" H 5100 5450 50  0001 C CNN
F 3 "" H 5100 5450 50  0001 C CNN
	1    5100 5450
	1    0    0    -1  
$EndComp
$Comp
L Device:R R4
U 1 1 5F04EB26
P 5100 5300
F 0 "R4" H 5170 5346 50  0000 L CNN
F 1 "100k" H 5170 5255 50  0000 L CNN
F 2 "AO_tht:R_Axial_DIN0207_L6.3mm_D2.5mm_P10.16mm_Horizontal" V 5030 5300 50  0001 C CNN
F 3 "~" H 5100 5300 50  0001 C CNN
	1    5100 5300
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR08
U 1 1 5F07E14F
P 5100 4650
F 0 "#PWR08" H 5100 4400 50  0001 C CNN
F 1 "GND" H 5105 4477 50  0000 C CNN
F 2 "" H 5100 4650 50  0001 C CNN
F 3 "" H 5100 4650 50  0001 C CNN
	1    5100 4650
	1    0    0    -1  
$EndComp
$Comp
L Device:R R3
U 1 1 5F04E1E9
P 5100 4500
F 0 "R3" H 5170 4546 50  0000 L CNN
F 1 "100k" H 5170 4455 50  0000 L CNN
F 2 "AO_tht:R_Axial_DIN0207_L6.3mm_D2.5mm_P10.16mm_Horizontal" V 5030 4500 50  0001 C CNN
F 3 "~" H 5100 4500 50  0001 C CNN
	1    5100 4500
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR07
U 1 1 5F07DC23
P 5100 3850
F 0 "#PWR07" H 5100 3600 50  0001 C CNN
F 1 "GND" H 5105 3677 50  0000 C CNN
F 2 "" H 5100 3850 50  0001 C CNN
F 3 "" H 5100 3850 50  0001 C CNN
	1    5100 3850
	1    0    0    -1  
$EndComp
$Comp
L Device:R R2
U 1 1 5F04DE0C
P 5100 3700
F 0 "R2" H 5170 3746 50  0000 L CNN
F 1 "100k" H 5170 3655 50  0000 L CNN
F 2 "AO_tht:R_Axial_DIN0207_L6.3mm_D2.5mm_P10.16mm_Horizontal" V 5030 3700 50  0001 C CNN
F 3 "~" H 5100 3700 50  0001 C CNN
	1    5100 3700
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR06
U 1 1 5F07D437
P 5100 3050
F 0 "#PWR06" H 5100 2800 50  0001 C CNN
F 1 "GND" H 5105 2877 50  0000 C CNN
F 2 "" H 5100 3050 50  0001 C CNN
F 3 "" H 5100 3050 50  0001 C CNN
	1    5100 3050
	1    0    0    -1  
$EndComp
$Comp
L Device:R R1
U 1 1 5F04D4E6
P 5100 2900
F 0 "R1" H 5170 2946 50  0000 L CNN
F 1 "100k" H 5170 2855 50  0000 L CNN
F 2 "AO_tht:R_Axial_DIN0207_L6.3mm_D2.5mm_P10.16mm_Horizontal" V 5030 2900 50  0001 C CNN
F 3 "~" H 5100 2900 50  0001 C CNN
	1    5100 2900
	1    0    0    -1  
$EndComp
Wire Wire Line
	6150 3600 6500 3600
Wire Wire Line
	6150 4400 6500 4400
Connection ~ 7250 6050
Wire Wire Line
	7250 6050 7250 6350
Wire Wire Line
	7250 5750 7250 6050
Connection ~ 7550 6350
$Comp
L power:GND #PWR015
U 1 1 5F5A7D51
P 7550 6350
AR Path="/5F5A7D51" Ref="#PWR015"  Part="1" 
AR Path="/5F1DEF77/5F5A7D51" Ref="#PWR?"  Part="1" 
F 0 "#PWR015" H 7550 6100 50  0001 C CNN
F 1 "GND" H 7555 6177 50  0000 C CNN
F 2 "" H 7550 6350 50  0001 C CNN
F 3 "" H 7550 6350 50  0001 C CNN
	1    7550 6350
	1    0    0    -1  
$EndComp
Wire Wire Line
	7250 6350 7550 6350
$Comp
L 4xxx:4066 U3
U 3 1 5F5A7D5C
P 7550 6050
AR Path="/5F5A7D5C" Ref="U3"  Part="3" 
AR Path="/5F1DEF77/5F5A7D5C" Ref="U?"  Part="3" 
F 0 "U3" V 7596 5922 50  0000 R CNN
F 1 "CD4066" V 7505 5922 50  0000 R CNN
F 2 "AO_tht:DIP-14_W7.62mm_Socket_LongPads" H 7550 6050 50  0001 C CNN
F 3 "http://www.ti.com/lit/ds/symlink/cd4066b.pdf" H 7550 6050 50  0001 C CNN
	3    7550 6050
	0    -1   -1   0   
$EndComp
Wire Wire Line
	7550 5750 7250 5750
$Comp
L Connector_Generic:Conn_02x07_Odd_Even J1
U 1 1 5F98391F
P 3150 3650
F 0 "J1" H 3150 4200 50  0000 L CNN
F 1 "2x7 IDC header" H 2950 4100 50  0000 L CNN
F 2 "Connector_IDC:IDC-Header_2x07_P2.54mm_Vertical" H 3150 3650 50  0001 C CNN
F 3 "~" H 3150 3650 50  0001 C CNN
F 4 "A-3349" H 3150 3650 50  0001 C CNN "SKU"
F 5 "Tayda" H 3150 3650 50  0001 C CNN "Vendor"
	1    3150 3650
	-1   0    0    -1  
$EndComp
Wire Wire Line
	6150 2800 6500 2800
Connection ~ 6150 2800
Text GLabel 8250 2850 2    50   Output ~ 0
MIXIN_+3
Text GLabel 8250 3200 2    50   Output ~ 0
MIXIN_+2
Text GLabel 8250 3650 2    50   Output ~ 0
MIXIN_+1
Text GLabel 8250 4000 2    50   Output ~ 0
MIXIN_0
Wire Wire Line
	6500 2800 6500 3100
Wire Wire Line
	6500 3600 6500 3900
Text GLabel 8250 4450 2    50   Output ~ 0
MIXIN_-1
Text GLabel 8250 4800 2    50   Output ~ 0
MIXIN_-2
Text GLabel 8250 5250 2    50   Output ~ 0
MIXIN_-3
Wire Wire Line
	6500 4400 6500 4700
Text GLabel 2850 3650 0    50   Input ~ 0
MIXIN_0
Text GLabel 2850 3850 0    50   Input ~ 0
MIXIN_-1
Text GLabel 2850 3950 0    50   Input ~ 0
MIXIN_-2
Text GLabel 2850 3750 0    50   Input ~ 0
MIXIN_-3
$Comp
L 4xxx:4066 U2
U 4 1 5F06441D
P 5700 3150
F 0 "U2" V 5746 3022 50  0000 R CNN
F 1 "CD4066" V 5655 3022 50  0000 R CNN
F 2 "AO_tht:DIP-14_W7.62mm_Socket_LongPads" H 5700 3150 50  0001 C CNN
F 3 "http://www.ti.com/lit/ds/symlink/cd4066b.pdf" H 5700 3150 50  0001 C CNN
	4    5700 3150
	0    -1   -1   0   
$EndComp
$Comp
L 4xxx:4066 U3
U 4 1 5F05DE66
P 4600 5450
F 0 "U3" V 4646 5322 50  0000 R CNN
F 1 "CD4066" V 4555 5322 50  0000 R CNN
F 2 "AO_tht:DIP-14_W7.62mm_Socket_LongPads" H 4600 5450 50  0001 C CNN
F 3 "http://www.ti.com/lit/ds/symlink/cd4066b.pdf" H 4600 5450 50  0001 C CNN
	4    4600 5450
	0    -1   -1   0   
$EndComp
$Comp
L 4xxx:4066 U2
U 3 1 5F055DD6
P 4600 3050
F 0 "U2" V 4646 2922 50  0000 R CNN
F 1 "CD4066" V 4555 2922 50  0000 R CNN
F 2 "AO_tht:DIP-14_W7.62mm_Socket_LongPads" H 4600 3050 50  0001 C CNN
F 3 "http://www.ti.com/lit/ds/symlink/cd4066b.pdf" H 4600 3050 50  0001 C CNN
	3    4600 3050
	0    -1   -1   0   
$EndComp
$Comp
L 4xxx:4066 U2
U 2 1 5F064423
P 5700 3950
F 0 "U2" V 5746 3822 50  0000 R CNN
F 1 "CD4066" V 5655 3822 50  0000 R CNN
F 2 "AO_tht:DIP-14_W7.62mm_Socket_LongPads" H 5700 3950 50  0001 C CNN
F 3 "http://www.ti.com/lit/ds/symlink/cd4066b.pdf" H 5700 3950 50  0001 C CNN
	2    5700 3950
	0    -1   -1   0   
$EndComp
Wire Wire Line
	4050 3550 4050 3850
Wire Wire Line
	3350 3450 5150 3450
Wire Wire Line
	3900 3050 3900 3350
Wire Wire Line
	3900 3350 3350 3350
Text GLabel 2850 3550 0    50   Input ~ 0
MIXIN_+1
Text GLabel 2850 3450 0    50   Input ~ 0
MIXIN_+2
Text GLabel 2850 3350 0    50   Input ~ 0
MIXIN_+3
Wire Wire Line
	3850 3950 3850 5050
Wire Wire Line
	3900 3850 3900 4650
Wire Wire Line
	3950 3750 3950 5450
Wire Wire Line
	3950 5450 4300 5450
Text Label 3350 3350 0    50   ~ 0
RIPOUT_+3
Text Label 3350 3450 0    50   ~ 0
RIPOUT_+2
Text Label 3350 3550 0    50   ~ 0
RIPOUT_+1
Text Label 3350 3650 0    50   ~ 0
RIPOUT_0
Text Label 3350 3750 0    50   ~ 0
RIPOUT_-3
Text Label 3350 3850 0    50   ~ 0
RIPOUT_-1
Text Label 3350 3950 0    50   ~ 0
RIPOUT_-2
Text Notes 1150 1800 0    50   ~ 0
This is used in conjunction with Barton's original PCB,\nbuilt with these changes:\n• Pots omitted\n• Output capacitors moved to pots pads 2-3\n• Ribbon cable mounted at capacitors pads\n• Output stage feedback resistor omitted, 100k* pot wired to its pads\n• Shrouded power header replaced by open pin header on underside\n• 10 uF rail to ground capacitors omitted\n• 10R resistors replaced with 0R (or jumpers)\n• 10 mm M3 nylon spacer glued to underside\n\n*10Vpp output with 0–10V CV. Use 200k pot with 0–5V CV.
Wire Wire Line
	6500 4700 6600 4700
Wire Wire Line
	6500 3900 6600 3900
Wire Wire Line
	6500 3100 6600 3100
$Comp
L AO_symbols:TL074 U1
U 2 1 61677A85
P 7750 3650
F 0 "U1" H 7950 3550 50  0000 C CNN
F 1 "TL074" H 7900 3450 50  0000 C CNN
F 2 "AO_tht:DIP-14_W7.62mm_Socket_LongPads" H 7700 3750 50  0001 C CNN
F 3 "" H 7800 3850 50  0001 C CNN
F 4 "Tayda" H 7750 3650 50  0001 C CNN "Vendor"
F 5 "A-1138" H 7750 3650 50  0001 C CNN "SKU"
	2    7750 3650
	1    0    0    -1  
$EndComp
Wire Wire Line
	6500 3450 7300 3450
Wire Wire Line
	7300 3450 7300 3200
Wire Wire Line
	7300 3200 7200 3200
Wire Wire Line
	6600 3300 6500 3300
Wire Wire Line
	6500 3300 6500 3450
$Comp
L AO_symbols:TL074 U1
U 1 1 616797EC
P 6900 4000
F 0 "U1" H 7100 3900 50  0000 C CNN
F 1 "TL074" H 7050 3800 50  0000 C CNN
F 2 "AO_tht:DIP-14_W7.62mm_Socket_LongPads" H 6850 4100 50  0001 C CNN
F 3 "" H 6950 4200 50  0001 C CNN
F 4 "Tayda" H 6900 4000 50  0001 C CNN "Vendor"
F 5 "A-1138" H 6900 4000 50  0001 C CNN "SKU"
	1    6900 4000
	1    0    0    -1  
$EndComp
Wire Wire Line
	7350 3100 8150 3100
Wire Wire Line
	8150 3100 8150 2850
Wire Wire Line
	8150 2850 8050 2850
Wire Wire Line
	7450 2950 7350 2950
Wire Wire Line
	7350 2950 7350 3100
$Comp
L AO_symbols:TL074 U1
U 4 1 6167B637
P 7750 2850
F 0 "U1" H 7950 2750 50  0000 C CNN
F 1 "TL074" H 7900 2650 50  0000 C CNN
F 2 "AO_tht:DIP-14_W7.62mm_Socket_LongPads" H 7700 2950 50  0001 C CNN
F 3 "" H 7800 3050 50  0001 C CNN
F 4 "Tayda" H 7750 2850 50  0001 C CNN "Vendor"
F 5 "A-1138" H 7750 2850 50  0001 C CNN "SKU"
	4    7750 2850
	1    0    0    -1  
$EndComp
Wire Wire Line
	6500 4250 7300 4250
Wire Wire Line
	7300 4250 7300 4000
Wire Wire Line
	7300 4000 7200 4000
Wire Wire Line
	6600 4100 6500 4100
Wire Wire Line
	6500 4100 6500 4250
$Comp
L AO_symbols:TL074 U1
U 3 1 6167DB32
P 6900 3200
F 0 "U1" H 7100 3100 50  0000 C CNN
F 1 "TL074" H 7050 3000 50  0000 C CNN
F 2 "AO_tht:DIP-14_W7.62mm_Socket_LongPads" H 6850 3300 50  0001 C CNN
F 3 "" H 6950 3400 50  0001 C CNN
F 4 "Tayda" H 6900 3200 50  0001 C CNN "Vendor"
F 5 "A-1138" H 6900 3200 50  0001 C CNN "SKU"
	3    6900 3200
	1    0    0    -1  
$EndComp
Wire Wire Line
	7350 3900 8150 3900
Wire Wire Line
	8150 3900 8150 3650
Wire Wire Line
	8150 3650 8050 3650
Wire Wire Line
	7450 3750 7350 3750
Wire Wire Line
	7350 3750 7350 3900
Wire Wire Line
	6500 5050 7300 5050
Wire Wire Line
	7300 5050 7300 4800
Wire Wire Line
	7300 4800 7200 4800
Wire Wire Line
	6600 4900 6500 4900
Wire Wire Line
	6500 4900 6500 5050
Wire Wire Line
	7350 4700 8150 4700
Wire Wire Line
	8150 4700 8150 4450
Wire Wire Line
	8150 4450 8050 4450
Wire Wire Line
	7450 4550 7350 4550
Wire Wire Line
	7350 4550 7350 4700
Wire Wire Line
	7350 5500 8150 5500
Wire Wire Line
	8150 5500 8150 5250
Wire Wire Line
	8150 5250 8050 5250
Wire Wire Line
	7450 5350 7350 5350
Wire Wire Line
	7350 5350 7350 5500
Wire Wire Line
	5600 6250 6400 6250
Wire Wire Line
	6400 6250 6400 6000
Wire Wire Line
	6400 6000 6300 6000
Wire Wire Line
	5700 6100 5600 6100
Wire Wire Line
	5600 6100 5600 6250
$Comp
L power:GND #PWR011
U 1 1 6169D179
P 5450 5950
F 0 "#PWR011" H 5450 5700 50  0001 C CNN
F 1 "GND" H 5455 5777 50  0000 C CNN
F 2 "" H 5450 5950 50  0001 C CNN
F 3 "" H 5450 5950 50  0001 C CNN
	1    5450 5950
	1    0    0    -1  
$EndComp
Wire Wire Line
	5450 5950 5450 5900
Wire Wire Line
	5450 5900 5700 5900
Wire Wire Line
	8250 2850 8150 2850
Connection ~ 8150 2850
Wire Wire Line
	8250 3200 7300 3200
Connection ~ 7300 3200
Wire Wire Line
	8250 3650 8150 3650
Connection ~ 8150 3650
Wire Wire Line
	8250 4000 7300 4000
Connection ~ 7300 4000
Wire Wire Line
	8250 4800 7300 4800
Connection ~ 7300 4800
Wire Wire Line
	8250 5250 8150 5250
Connection ~ 8150 5250
$Comp
L AO_symbols:TL074 U1
U 5 1 616B6FA4
P 4050 7000
F 0 "U1" H 4100 7200 50  0000 C CNN
F 1 "TL074" H 4150 7100 50  0000 C CNN
F 2 "AO_tht:DIP-14_W7.62mm_Socket_LongPads" H 4000 7100 50  0001 C CNN
F 3 "" H 4100 7200 50  0001 C CNN
F 4 "Tayda" H 4050 7000 50  0001 C CNN "Vendor"
F 5 "A-1138" H 4050 7000 50  0001 C CNN "SKU"
	5    4050 7000
	1    0    0    -1  
$EndComp
$Comp
L AO_symbols:TL074 U4
U 5 1 616B7F3E
P 5200 7000
F 0 "U4" H 5250 7200 50  0000 C CNN
F 1 "TL074" H 5300 7100 50  0000 C CNN
F 2 "AO_tht:DIP-14_W7.62mm_Socket_LongPads" H 5150 7100 50  0001 C CNN
F 3 "" H 5250 7200 50  0001 C CNN
F 4 "Tayda" H 5200 7000 50  0001 C CNN "Vendor"
F 5 "A-1138" H 5200 7000 50  0001 C CNN "SKU"
	5    5200 7000
	1    0    0    -1  
$EndComp
$Comp
L AO_symbols:C C1
U 1 1 616B98F0
P 3500 6850
F 0 "C1" H 3615 6896 50  0000 L CNN
F 1 "100nF" H 3615 6805 50  0000 L CNN
F 2 "AO_tht:C_Disc_D3.0mm_W1.6mm_P2.50mm" H 3538 6700 50  0001 C CNN
F 3 "" H 3500 6850 50  0001 C CNN
F 4 "Tayda" H 3500 6850 50  0001 C CNN "Vendor"
	1    3500 6850
	1    0    0    -1  
$EndComp
$Comp
L AO_symbols:C C2
U 1 1 616BA1FB
P 3500 7150
F 0 "C2" H 3615 7196 50  0000 L CNN
F 1 "100nF" H 3615 7105 50  0000 L CNN
F 2 "AO_tht:C_Disc_D3.0mm_W1.6mm_P2.50mm" H 3538 7000 50  0001 C CNN
F 3 "" H 3500 7150 50  0001 C CNN
F 4 "Tayda" H 3500 7150 50  0001 C CNN "Vendor"
	1    3500 7150
	1    0    0    -1  
$EndComp
Wire Wire Line
	3500 6700 3950 6700
Wire Wire Line
	3500 7300 3950 7300
Wire Wire Line
	3500 7000 4100 7000
Wire Wire Line
	4100 7000 4100 7100
Connection ~ 3500 7000
$Comp
L power:GND #PWR03
U 1 1 616C41C0
P 4100 7100
F 0 "#PWR03" H 4100 6850 50  0001 C CNN
F 1 "GND" H 4105 6927 50  0000 C CNN
F 2 "" H 4100 7100 50  0001 C CNN
F 3 "" H 4100 7100 50  0001 C CNN
	1    4100 7100
	1    0    0    -1  
$EndComp
$Comp
L power:+12V #PWR01
U 1 1 616C4B17
P 3500 6700
F 0 "#PWR01" H 3500 6550 50  0001 C CNN
F 1 "+12V" H 3515 6873 50  0000 C CNN
F 2 "" H 3500 6700 50  0001 C CNN
F 3 "" H 3500 6700 50  0001 C CNN
	1    3500 6700
	1    0    0    -1  
$EndComp
Connection ~ 3500 6700
$Comp
L power:-12V #PWR02
U 1 1 616C56A4
P 3500 7300
F 0 "#PWR02" H 3500 7400 50  0001 C CNN
F 1 "-12V" H 3515 7473 50  0000 C CNN
F 2 "" H 3500 7300 50  0001 C CNN
F 3 "" H 3500 7300 50  0001 C CNN
	1    3500 7300
	-1   0    0    1   
$EndComp
Connection ~ 3500 7300
$Comp
L AO_symbols:C C3
U 1 1 616C82D3
P 4650 6850
F 0 "C3" H 4765 6896 50  0000 L CNN
F 1 "100nF" H 4765 6805 50  0000 L CNN
F 2 "AO_tht:C_Disc_D3.0mm_W1.6mm_P2.50mm" H 4688 6700 50  0001 C CNN
F 3 "" H 4650 6850 50  0001 C CNN
F 4 "Tayda" H 4650 6850 50  0001 C CNN "Vendor"
	1    4650 6850
	1    0    0    -1  
$EndComp
$Comp
L AO_symbols:C C4
U 1 1 616C82DA
P 4650 7150
F 0 "C4" H 4765 7196 50  0000 L CNN
F 1 "100nF" H 4765 7105 50  0000 L CNN
F 2 "AO_tht:C_Disc_D3.0mm_W1.6mm_P2.50mm" H 4688 7000 50  0001 C CNN
F 3 "" H 4650 7150 50  0001 C CNN
F 4 "Tayda" H 4650 7150 50  0001 C CNN "Vendor"
	1    4650 7150
	1    0    0    -1  
$EndComp
Wire Wire Line
	4650 6700 5100 6700
Wire Wire Line
	4650 7300 5100 7300
Wire Wire Line
	4650 7000 5250 7000
Wire Wire Line
	5250 7000 5250 7100
Connection ~ 4650 7000
$Comp
L power:GND #PWR010
U 1 1 616C82E5
P 5250 7100
F 0 "#PWR010" H 5250 6850 50  0001 C CNN
F 1 "GND" H 5255 6927 50  0000 C CNN
F 2 "" H 5250 7100 50  0001 C CNN
F 3 "" H 5250 7100 50  0001 C CNN
	1    5250 7100
	1    0    0    -1  
$EndComp
$Comp
L power:+12V #PWR04
U 1 1 616C82EB
P 4650 6700
F 0 "#PWR04" H 4650 6550 50  0001 C CNN
F 1 "+12V" H 4665 6873 50  0000 C CNN
F 2 "" H 4650 6700 50  0001 C CNN
F 3 "" H 4650 6700 50  0001 C CNN
	1    4650 6700
	1    0    0    -1  
$EndComp
Connection ~ 4650 6700
$Comp
L power:-12V #PWR05
U 1 1 616C82F2
P 4650 7300
F 0 "#PWR05" H 4650 7400 50  0001 C CNN
F 1 "-12V" H 4665 7473 50  0000 C CNN
F 2 "" H 4650 7300 50  0001 C CNN
F 3 "" H 4650 7300 50  0001 C CNN
	1    4650 7300
	-1   0    0    1   
$EndComp
Connection ~ 4650 7300
$Comp
L AO_symbols:TL074 U4
U 3 1 61688045
P 6000 6000
F 0 "U4" H 6200 5900 50  0000 C CNN
F 1 "TL074" H 6150 5800 50  0000 C CNN
F 2 "AO_tht:DIP-14_W7.62mm_Socket_LongPads" H 5950 6100 50  0001 C CNN
F 3 "" H 6050 6200 50  0001 C CNN
F 4 "Tayda" H 6000 6000 50  0001 C CNN "Vendor"
F 5 "A-1138" H 6000 6000 50  0001 C CNN "SKU"
	3    6000 6000
	1    0    0    -1  
$EndComp
$Comp
L AO_symbols:TL074 U4
U 2 1 6168003A
P 6900 4800
F 0 "U4" H 7100 4700 50  0000 C CNN
F 1 "TL074" H 7050 4600 50  0000 C CNN
F 2 "AO_tht:DIP-14_W7.62mm_Socket_LongPads" H 6850 4900 50  0001 C CNN
F 3 "" H 6950 5000 50  0001 C CNN
F 4 "Tayda" H 6900 4800 50  0001 C CNN "Vendor"
F 5 "A-1138" H 6900 4800 50  0001 C CNN "SKU"
	2    6900 4800
	1    0    0    -1  
$EndComp
$Comp
L AO_symbols:TL074 U4
U 4 1 6169AA8C
P 7750 5250
F 0 "U4" H 7950 5150 50  0000 C CNN
F 1 "TL074" H 7900 5050 50  0000 C CNN
F 2 "AO_tht:DIP-14_W7.62mm_Socket_LongPads" H 7700 5350 50  0001 C CNN
F 3 "" H 7800 5450 50  0001 C CNN
F 4 "Tayda" H 7750 5250 50  0001 C CNN "Vendor"
F 5 "A-1138" H 7750 5250 50  0001 C CNN "SKU"
	4    7750 5250
	1    0    0    -1  
$EndComp
$Comp
L AO_symbols:TL074 U4
U 1 1 61682DF1
P 7750 4450
F 0 "U4" H 7950 4350 50  0000 C CNN
F 1 "TL074" H 7900 4250 50  0000 C CNN
F 2 "AO_tht:DIP-14_W7.62mm_Socket_LongPads" H 7700 4550 50  0001 C CNN
F 3 "" H 7800 4650 50  0001 C CNN
F 4 "Tayda" H 7750 4450 50  0001 C CNN "Vendor"
F 5 "A-1138" H 7750 4450 50  0001 C CNN "SKU"
	1    7750 4450
	1    0    0    -1  
$EndComp
Text Notes 5700 2550 0    50   ~ 0
Pulldowns need to be >~~ 100k because in parallel with bottom \nof CV attenuators, but ~~ 1 M slows down the edge too much. \nPulldowns >~~ 10k don't work if connected direct to capacitor \n(on Barton board). Op amp buffers needed to fix that. 100k \nseems optimal.
Wire Wire Line
	2225 7000 2225 7525
Wire Wire Line
	775  7000 775  7525
Connection ~ 2225 7525
Connection ~ 775  7525
Connection ~ 2225 6525
Wire Wire Line
	2225 6525 2900 6525
Wire Wire Line
	2225 6700 2225 6525
Wire Wire Line
	2225 7525 2900 7525
$Comp
L power:+12V #PWR?
U 1 1 6160CD92
P 2225 6525
AR Path="/5F1DEF77/5F25E2E1/6160CD92" Ref="#PWR?"  Part="1" 
AR Path="/5F1DEF77/6160CD92" Ref="#PWR?"  Part="1" 
AR Path="/6160CD92" Ref="#PWR0101"  Part="1" 
F 0 "#PWR0101" H 2225 6375 50  0001 C CNN
F 1 "+12V" H 2240 6698 50  0000 C CNN
F 2 "" H 2225 6525 50  0001 C CNN
F 3 "" H 2225 6525 50  0001 C CNN
	1    2225 6525
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR?
U 1 1 6160CD98
P 2225 7525
AR Path="/5F1DEF77/5F25E2E1/6160CD98" Ref="#PWR?"  Part="1" 
AR Path="/5F1DEF77/6160CD98" Ref="#PWR?"  Part="1" 
AR Path="/6160CD98" Ref="#PWR0102"  Part="1" 
F 0 "#PWR0102" H 2225 7275 50  0001 C CNN
F 1 "GND" H 2230 7352 50  0000 C CNN
F 2 "" H 2225 7525 50  0001 C CNN
F 3 "" H 2225 7525 50  0001 C CNN
	1    2225 7525
	1    0    0    -1  
$EndComp
$Comp
L AO_symbols:C C?
U 1 1 6160CDA0
P 2225 6850
AR Path="/5F1DEF77/5F25E2E1/6160CDA0" Ref="C?"  Part="1" 
AR Path="/5F1DEF77/6160CDA0" Ref="C?"  Part="1" 
AR Path="/6160CDA0" Ref="C6"  Part="1" 
F 0 "C6" H 2340 6896 50  0000 L CNN
F 1 "100nF" H 2340 6805 50  0000 L CNN
F 2 "AO_tht:C_Disc_D3.0mm_W1.6mm_P2.50mm" H 2263 6700 50  0001 C CNN
F 3 "~" H 2225 6850 50  0001 C CNN
F 4 "A-553" H 2225 6850 50  0001 C CNN "SKU"
F 5 "Tayda" H 2225 6850 50  0001 C CNN "Vendor"
	1    2225 6850
	1    0    0    -1  
$EndComp
$Comp
L 4xxx:4066 U?
U 5 1 6160CDA6
P 2900 7025
AR Path="/5F1DEF77/6160CDA6" Ref="U?"  Part="5" 
AR Path="/6160CDA6" Ref="U3"  Part="5" 
F 0 "U3" H 3130 7071 50  0000 L CNN
F 1 "CD4066" H 3130 6980 50  0000 L CNN
F 2 "AO_tht:DIP-14_W7.62mm_Socket_LongPads" H 2900 7025 50  0001 C CNN
F 3 "http://www.ti.com/lit/ds/symlink/cd4066b.pdf" H 2900 7025 50  0001 C CNN
	5    2900 7025
	1    0    0    -1  
$EndComp
Connection ~ 775  6525
Wire Wire Line
	775  6525 1450 6525
Wire Wire Line
	775  6700 775  6525
Wire Wire Line
	775  7525 1450 7525
$Comp
L power:+12V #PWR?
U 1 1 6160CDB0
P 775 6525
AR Path="/5F1DEF77/5F25E2E1/6160CDB0" Ref="#PWR?"  Part="1" 
AR Path="/5F1DEF77/6160CDB0" Ref="#PWR?"  Part="1" 
AR Path="/6160CDB0" Ref="#PWR0103"  Part="1" 
F 0 "#PWR0103" H 775 6375 50  0001 C CNN
F 1 "+12V" H 790 6698 50  0000 C CNN
F 2 "" H 775 6525 50  0001 C CNN
F 3 "" H 775 6525 50  0001 C CNN
	1    775  6525
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR?
U 1 1 6160CDB6
P 775 7525
AR Path="/5F1DEF77/5F25E2E1/6160CDB6" Ref="#PWR?"  Part="1" 
AR Path="/5F1DEF77/6160CDB6" Ref="#PWR?"  Part="1" 
AR Path="/6160CDB6" Ref="#PWR0104"  Part="1" 
F 0 "#PWR0104" H 775 7275 50  0001 C CNN
F 1 "GND" H 780 7352 50  0000 C CNN
F 2 "" H 775 7525 50  0001 C CNN
F 3 "" H 775 7525 50  0001 C CNN
	1    775  7525
	1    0    0    -1  
$EndComp
$Comp
L AO_symbols:C C?
U 1 1 6160CDBE
P 775 6850
AR Path="/5F1DEF77/5F25E2E1/6160CDBE" Ref="C?"  Part="1" 
AR Path="/5F1DEF77/6160CDBE" Ref="C?"  Part="1" 
AR Path="/6160CDBE" Ref="C5"  Part="1" 
F 0 "C5" H 890 6896 50  0000 L CNN
F 1 "100nF" H 890 6805 50  0000 L CNN
F 2 "AO_tht:C_Disc_D3.0mm_W1.6mm_P2.50mm" H 813 6700 50  0001 C CNN
F 3 "~" H 775 6850 50  0001 C CNN
F 4 "A-553" H 775 6850 50  0001 C CNN "SKU"
F 5 "Tayda" H 775 6850 50  0001 C CNN "Vendor"
	1    775  6850
	1    0    0    -1  
$EndComp
$Comp
L 4xxx:4066 U?
U 5 1 6160CDC4
P 1450 7025
AR Path="/5F1DEF77/6160CDC4" Ref="U?"  Part="5" 
AR Path="/6160CDC4" Ref="U2"  Part="5" 
F 0 "U2" H 1680 7071 50  0000 L CNN
F 1 "CD4066" H 1680 6980 50  0000 L CNN
F 2 "AO_tht:DIP-14_W7.62mm_Socket_LongPads" H 1450 7025 50  0001 C CNN
F 3 "http://www.ti.com/lit/ds/symlink/cd4066b.pdf" H 1450 7025 50  0001 C CNN
	5    1450 7025
	1    0    0    -1  
$EndComp
Wire Wire Line
	3350 3550 4050 3550
Wire Wire Line
	3350 3650 4000 3650
Wire Wire Line
	3350 3750 3950 3750
Wire Wire Line
	3350 3850 3900 3850
Wire Wire Line
	3350 3950 3850 3950
Wire Wire Line
	8150 4450 8250 4450
Connection ~ 8150 4450
Connection ~ 5100 2750
Wire Wire Line
	5100 2750 7450 2750
Wire Wire Line
	4600 2750 5100 2750
Connection ~ 5100 3550
Wire Wire Line
	5100 3550 7450 3550
Wire Wire Line
	4600 3550 5100 3550
Connection ~ 5100 4350
Wire Wire Line
	5100 4350 7450 4350
Wire Wire Line
	4600 4350 5100 4350
Connection ~ 5100 5150
Wire Wire Line
	5100 5150 7450 5150
Wire Wire Line
	4600 5150 5100 5150
$EndSCHEMATC
